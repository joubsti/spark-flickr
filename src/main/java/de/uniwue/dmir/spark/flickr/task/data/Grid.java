package de.uniwue.dmir.spark.flickr.task.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import de.uniwue.dmir.swd.core.data.IMergeable;
import de.uniwue.dmir.swd.core.features.parser.IJsonParsable;
import de.uniwue.dmir.swd.core.features.util.intervalizer.DegreeIntervalizer;

public class Grid implements IMergeable<Grid>, KryoSerializable, Serializable, IJsonParsable{

	private static final long serialVersionUID = 8094162034239416829L;
	private HashMap<Integer, GridCell> elements;
	private DegreeIntervalizer intervalizer;
	
	public Grid(DegreeIntervalizer intervalizer) {
		this.elements = new HashMap<Integer, GridCell>();
		this.intervalizer = intervalizer;
	}
	
	public Grid merge(Integer index){
		
		if( elements.containsKey(index) ){
			elements.get(index).increaseCount();
		} else {
			elements.put(index, new GridCell(index));
		}
	    
	    return this;
	}
	
	public Grid merge(GridCell cell){
	
		int index = cell.getIndex();
		
		if( elements.containsKey(index) ){
			elements.get(index).merge(cell);
		} else {
			elements.put(index, cell);
		}
	    
	    return this;
	}

	public void read(Kryo kryo, Input input) {
		elements = new HashMap<Integer, GridCell>();
		int size = input.readInt();
		
		for( int i = 0; i < size; i++){
			GridCell cell = kryo.readObject(input, GridCell.class);
			elements.put(cell.getIndex(), cell);
		}
		
		intervalizer = kryo.readObject(input, DegreeIntervalizer.class);
	}

	public void write(Kryo kryo, Output output) {	
		Iterator<Integer> keys = elements.keySet().iterator();
		output.writeInt(elements.keySet().size());
	    while (keys.hasNext()) {
	    	int id = keys.next();
	    	kryo.writeObject(output, elements.get(id));
	    }
	    
	    kryo.writeObject(output, intervalizer);
	}

	public Grid merge(Grid other) {
		Iterator<Integer> keys = other.elements.keySet().iterator();
	    while (keys.hasNext()) {
	    	int id = keys.next();
	    	if( elements.containsKey(id) ){
	    		elements.get(id).merge(other.elements.get(id));
	    	} else {
	    		elements.put(id, other.elements.get(id));
	    	}
	    }
	    
	    return this;
	}
	
	public static Grid createByIndices(List<Integer> ids, DegreeIntervalizer intervalizer){
		Grid list = new Grid(intervalizer);
		
		for(Integer id: ids){
			list.merge(new GridCell(id));
		}
		
		return list;
	}

	public String parseToJson() {

		Map<String, Object> config = new HashMap<String, Object>();
		JsonBuilderFactory factory = Json.createBuilderFactory(config);

		JsonArrayBuilder cellsArrayBuilder = factory.createArrayBuilder();
		
		Iterator<Integer> keys = elements.keySet().iterator();
	    while (keys.hasNext()) {
	    	int id = keys.next();
	    	cellsArrayBuilder.add(elements.get(id).toJsonValue(intervalizer));
	    }
		/*for( int i = 0; i < elements.size(); i++){
			cellsArrayBuilder.add(this.elements.get(i).toJsonValue(intervalizer));
		}*/
		
		return cellsArrayBuilder.build().toString();
	}

}