package de.uniwue.dmir.spark.flickr.task;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.storage.StorageLevel;

import de.uniwue.dmir.spark.flickr.task.data.Grid;
import de.uniwue.dmir.swd.core.features.primitives.BoundingBox;
import de.uniwue.dmir.swd.core.features.util.intervalizer.DegreeIntervalizer;
import de.uniwue.dmir.swd.core.request.SparkRequest;
import de.uniwue.dmir.swd.core.request.parameter.ResultParser;
import de.uniwue.dmir.swd.core.request.parameter.StringValue;
import de.uniwue.dmir.swd.core.task.DataFrameToBoundingBoxTask;
import de.uniwue.dmir.swd.core.task.SparkTask;
import de.uniwue.dmir.swd.core.task.data.DataFrameStorage;
import de.uniwue.dmir.swd.core.task.data.TaskData;

public class FlickrPhotoCountTask extends SparkTask {
	
	private static final long serialVersionUID = -285022454498333187L;
	
	public FlickrPhotoCountTask(TaskData data) {
		super(data);
	}

	public String process(SparkRequest request){
		
		ResultParser parserParam =
				(ResultParser) request.getParameter(ResultParser.PARAM_RESULTPARSER);
		
		
		StringValue dataframeParameter = (StringValue) request.getParameter("dataframe");
		String dataFrameName = dataframeParameter.getValue();
		
		String parquetPath = (String) getData().get("parquetPath");

		DataFrameStorage dfs = (DataFrameStorage) getData().get("dataframes");
		DataFrame df;
		
		double diffLat = 0.0025;
		double diffLon = 0.0025;
		
		if( dfs.contains(dataFrameName)){
			df = dfs.getFrame(dataFrameName);
		} else {
			SQLContext sqlc = (SQLContext) getData().get("sqlcontext");
			df = sqlc.read().parquet(parquetPath + "/" + dataFrameName).persist(StorageLevel.MEMORY_AND_DISK());
			dfs.add(dataFrameName, df);
		}
		
		//get bounding box
		BoundingBox bbox = dfs.getEntry(dataFrameName).getBoundingBox();
		if( bbox == null){
			TaskData td = new TaskData();
			td.put("dataframe", df);
			DataFrameToBoundingBoxTask dftbbt = new DataFrameToBoundingBoxTask(td);
			bbox = dftbbt.process(request);
			bbox.normalizeFor(diffLat, diffLon);
			dfs.getEntry(dataFrameName).setBoundingBox(bbox);
		}
		
		final DegreeIntervalizer intervalizer = new DegreeIntervalizer(bbox, diffLon, diffLat);
		 
		JavaRDD<Row> jrd = df.toJavaRDD();

		Grid cells = jrd.map(new Function<Row, Grid>(){

			private static final long serialVersionUID = 3503995616409045238L;

			public Grid call(Row row) throws Exception {
				double longitude = row.isNullAt(row.fieldIndex("longitude")) ? 0 : row.getDouble(row.fieldIndex("longitude"));
				double latitude  = row.isNullAt(row.fieldIndex("latitude")) ? 0 : row.getDouble(row.fieldIndex("latitude"));
				
				Integer id = intervalizer.toIntervalId(latitude, longitude);
				Grid cells = new Grid(intervalizer);
				cells.merge(id);
						
				return cells;
			}
			
		}).aggregate(new Grid(intervalizer), new Function2<Grid,Grid,Grid>(){
			private static final long serialVersionUID = -6962255146632303350L;

			public Grid call(Grid v1, Grid v2) throws Exception {
				
				v1.merge(v2);
				return v1;
			}
			
		}, new Function2<Grid,Grid,Grid>(){
			private static final long serialVersionUID = 5780837089396753854L;

			public Grid call(Grid v1, Grid v2) throws Exception {
				return v1.merge(v2);
			}
			
		});
		
		return parserParam.getParser().parse(cells);
	}
}