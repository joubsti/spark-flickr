package de.uniwue.dmir.spark.flickr.task;

import java.sql.Timestamp;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.storage.StorageLevel;

import de.uniwue.dmir.spark.flickr.task.data.Grid;
import de.uniwue.dmir.spark.flickr.task.data.OwnerDateTaken;
import de.uniwue.dmir.spark.flickr.task.data.TransitionMap;
import de.uniwue.dmir.swd.core.features.primitives.BoundingBox;
import de.uniwue.dmir.swd.core.features.util.intervalizer.DegreeIntervalizer;
import de.uniwue.dmir.swd.core.request.SparkRequest;
import de.uniwue.dmir.swd.core.request.parameter.ResultParser;
import de.uniwue.dmir.swd.core.request.parameter.StringValue;
import de.uniwue.dmir.swd.core.task.DataFrameToBoundingBoxTask;
import de.uniwue.dmir.swd.core.task.SparkTask;
import de.uniwue.dmir.swd.core.task.data.DataFrameStorage;
import de.uniwue.dmir.swd.core.task.data.TaskData;

public class FlickrTransitionCountTask extends SparkTask {
	
	private static final long serialVersionUID = -285022454498333187L;
	
	public FlickrTransitionCountTask(TaskData data) {
		super(data);
	}

	public String process(SparkRequest request){
		
		ResultParser parserParam =
				(ResultParser) request.getParameter(ResultParser.PARAM_RESULTPARSER);
		
		StringValue dataframeParameter = (StringValue) request.getParameter("dataframe");
		String dataFrameName = dataframeParameter.getValue();
		
		String parquetPath = (String) getData().get("parquetPath");
		
		final TransitionMap.TransitionType transitionType;
		
		Integer bboxindex = 0;
		
		if( request.getRequest().getParameter("from") != null){
			String from = request.getRequest().getParameter("from");
			bboxindex = Integer.parseInt(from);
			transitionType = TransitionMap.TransitionType.FROM;
		} else if( request.getRequest().getParameter("to") != null){
			String to = request.getRequest().getParameter("to");
			bboxindex = Integer.parseInt(to);
			transitionType = TransitionMap.TransitionType.TO;
		} else {
			String from_and_to = request.getRequest().getParameter("from_and_to");
			bboxindex = Integer.parseInt(from_and_to);
			transitionType = TransitionMap.TransitionType.FROM_AND_TO;
		}
		
		DataFrameStorage dfs = (DataFrameStorage) getData().get("dataframes");
		DataFrame df;
		
		double diffLat = 0.0025;
		double diffLon = 0.0025;
		
		if( dfs.contains(dataFrameName)){
			df = dfs.getFrame(dataFrameName);
		} else {
			SQLContext sqlc = (SQLContext) getData().get("sqlcontext");
			df = sqlc.read().parquet(parquetPath + "/" + dataFrameName).persist(StorageLevel.MEMORY_AND_DISK());
			dfs.add(dataFrameName, df);
		}
		
		//get bounding box
		BoundingBox bbox = dfs.getEntry(dataFrameName).getBoundingBox();
		if( bbox == null){
			TaskData td = new TaskData();
			td.put("dataframe", df);
			DataFrameToBoundingBoxTask dftbbt = new DataFrameToBoundingBoxTask(td);
			bbox = dftbbt.process(request);
			bbox.normalizeFor(diffLat, diffLon);
			dfs.getEntry(dataFrameName).setBoundingBox(bbox);
		}
		
		final DegreeIntervalizer intervalizer = new DegreeIntervalizer(bbox, diffLon, diffLat);
		BoundingBox bb = intervalizer.toBoundingBox(bboxindex);
		
		// build the following structure out of all flickr pictures within the given bounding box
		// owner[]-> PhotoTransition -> Timestamp 	datetaken
		//			 					GridCell 	transitionFrom
		//			 					GridCell 	transitionTo
		// the primary attribute set is datetaken
		final OwnerDateTaken ownerDates = bb.applyToDataFrame(df).select("owner", "datetaken").toJavaRDD().map(new Function<Row, OwnerDateTaken>(){
			private static final long serialVersionUID = -8658361020549757144L;

			public OwnerDateTaken call(Row v1) throws Exception {
				Timestamp datetaken  = v1.isNullAt(v1.fieldIndex("datetaken")) ? new Timestamp(0) : v1.getTimestamp(v1.fieldIndex("datetaken"));
				return new OwnerDateTaken(v1.getString(v1.fieldIndex("owner")), datetaken);
			}
			
		}).aggregate(new OwnerDateTaken(), new Function2<OwnerDateTaken,OwnerDateTaken,OwnerDateTaken>(){
			private static final long serialVersionUID = -5277292609789155817L;

			public OwnerDateTaken call(OwnerDateTaken v1, OwnerDateTaken v2) throws Exception {
				
				v1.merge(v2);
				return v1;
			}
			
		}, new Function2<OwnerDateTaken,OwnerDateTaken,OwnerDateTaken>(){
			private static final long serialVersionUID = 7742419050576468815L;

			public OwnerDateTaken call(OwnerDateTaken v1, OwnerDateTaken v2) throws Exception {
				return v1.merge(v2);
			}
			
		});
		
		if( ownerDates.getTransitions().keySet().size() == 0 )
			return parserParam.getParser().parse(new Grid(intervalizer));
		
		OwnerDateTaken transitionEndpoints = df.toJavaRDD().map(new Function<Row, OwnerDateTaken>(){

			private static final long serialVersionUID = 7758803924897012107L;

			public OwnerDateTaken call(Row v1) throws Exception {
				
				OwnerDateTaken ret = null;
				
				int ownerFieldIndex = v1.fieldIndex("owner");
				
				if( v1.isNullAt(v1.fieldIndex("datetaken")) ){
					return ret;
				}
				
				String owner = v1.getString(ownerFieldIndex);
				
				if( ownerDates.getTransitions().containsKey(owner)){
					TransitionMap transitions = ownerDates.getTransitions().get(owner);
					
					Timestamp dateTaken = v1.getTimestamp(v1.fieldIndex("datetaken"));
					
					if( !transitions.getEntries().containsKey(dateTaken.getTime())){
						// do not return a OwnerDateTaken object if selected bounding box contains already a entry
						// of the same owner and date taken
						double longitude = v1.isNullAt(v1.fieldIndex("longitude")) ? 0 : v1.getDouble(v1.fieldIndex("longitude"));
						double latitude  = v1.isNullAt(v1.fieldIndex("latitude")) ? 0 : v1.getDouble(v1.fieldIndex("latitude"));
						Integer index = intervalizer.toIntervalId(latitude, longitude);
						ret = new OwnerDateTaken(owner, dateTaken, index);
					}
				}
				
				return ret;
			}
	
		}).aggregate(new OwnerDateTaken(), new Function2<OwnerDateTaken,OwnerDateTaken,OwnerDateTaken>(){
			private static final long serialVersionUID = -6962255146632303350L;

			public OwnerDateTaken call(OwnerDateTaken v1, OwnerDateTaken v2) throws Exception {
				
				if( v1 == null && v2 == null)
					return null;
				
				if( v1 == null )
					return ownerDates.remainingMerged(v2, transitionType);
				
				if( v2 == null )
					return ownerDates.remainingMerged(v1, transitionType);
				
				v1.merge(v2);
				return ownerDates.remainingMerged(v1, transitionType);
			}
			
		}, new Function2<OwnerDateTaken,OwnerDateTaken,OwnerDateTaken>(){
			private static final long serialVersionUID = 5780837089396753854L;

			public OwnerDateTaken call(OwnerDateTaken v1, OwnerDateTaken v2) throws Exception {
				
				if( v1 == null && v2 == null)
					return null;
				
				if( v1 == null )
					return ownerDates.remainingMerged(v2, transitionType);
				
				if( v2 == null )
					return ownerDates.remainingMerged(v1, transitionType);
				
				v1.merge(v2);
				return ownerDates.remainingMerged(v1, transitionType);
			}
		});
	
		if( transitionEndpoints != null){
			return parserParam.getParser().parse(transitionEndpoints.toGrid(intervalizer));
		} else {
			return parserParam.getParser().parse(new Grid(intervalizer));
		}
	}
}
