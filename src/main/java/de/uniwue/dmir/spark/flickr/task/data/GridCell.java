package de.uniwue.dmir.spark.flickr.task.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonValue;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import de.uniwue.dmir.swd.core.data.IMergeable;
import de.uniwue.dmir.swd.core.features.primitives.Coordinate;
import de.uniwue.dmir.swd.core.features.util.intervalizer.DegreeIntervalizer;

public class GridCell implements IMergeable<GridCell>, KryoSerializable, Serializable{

	private static final long serialVersionUID = 1516587054500005795L;
	private long count;
	private int index;
	private Timestamp dateTaken;
	
	public GridCell(int index/*, DegreeIntervalizer intervalizer*/){
		this.index = index;
		this.count = 1;
	}
	
	public int getIndex(){
		return index;
	}
	
	public void increaseCount(){
		count++;
	}
	
	public void setDateTaken(Timestamp dateTaken){
		this.dateTaken = dateTaken;
	}
	
	public Timestamp getDateTaken(){
		return dateTaken;
	}
	
	public GridCell merge(GridCell other){
		this.count += other.count;
		return this;
	}
	
	public JsonValue toJsonValue(DegreeIntervalizer intervalizer){
		Map<String, Object> config = new HashMap<String, Object>();
		JsonBuilderFactory factory = Json.createBuilderFactory(config);
		
		Coordinate coordinates = intervalizer.fromIntervalId(index);
		
		JsonObject value = factory.createObjectBuilder()
			.add("count", count)
	        .add("latitude", coordinates.getLatitude())
	        .add("longitude", coordinates.getLongitude())
	        .add("bbox", intervalizer.toBoundingBox(index).toJsonArray())
	        .add("index", index)
	        .build();
		return value;
	}

	public void read(Kryo kryo, Input input) {
		// TODO Auto-generated method stub
		count = input.readLong();
		index = input.readInt();
	}

	public void write(Kryo kryo, Output output) {
		// TODO Auto-generated method stub
		output.writeLong(count);
		output.writeInt(index);
	}
}