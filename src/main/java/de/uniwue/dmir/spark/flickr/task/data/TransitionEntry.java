package de.uniwue.dmir.spark.flickr.task.data;

import java.io.Serializable;
import java.sql.Timestamp;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class TransitionEntry implements KryoSerializable, Serializable {
	
	private static final long serialVersionUID = -6431872742026366632L;
	private int index;
	private Timestamp dateTaken;
	
	public TransitionEntry(Timestamp dateTaken){
		index = -1;
		this.dateTaken = dateTaken;
	}
	
	public TransitionEntry(Timestamp dateTaken, int index){
		this.dateTaken = dateTaken;
		this.index = index;
	}
	
	public int getIndex(){
		return index;
	}

	public void write(Kryo kryo, Output output) {
		output.writeInt(index);
		output.writeLong(dateTaken.getTime());
	}

	public void read(Kryo kryo, Input input) {
		index = input.readInt();
		dateTaken = new Timestamp(input.readLong());
	}
	
}
