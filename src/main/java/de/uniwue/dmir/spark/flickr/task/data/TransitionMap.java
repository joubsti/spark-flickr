package de.uniwue.dmir.spark.flickr.task.data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class TransitionMap  implements KryoSerializable, Serializable{

	private static final long serialVersionUID = -5452832042245214616L;
	private TreeMap<Long, TransitionEntry> entries;

	public enum TransitionType{
		FROM,
		TO,
		FROM_AND_TO
	}
	
	public TransitionMap(){
		entries = new TreeMap<Long, TransitionEntry>();
	}

	public void write(Kryo kryo, Output output) {
		Iterator<Long> keys = entries.keySet().iterator();
		output.writeInt(entries.keySet().size());
	    while (keys.hasNext()) {
	    	Long timestamp = keys.next();
	    	output.writeLong(timestamp);
	    	kryo.writeObject(output, entries.get(timestamp));
	    }
	}

	public void read(Kryo kryo, Input input) {
		
		int size = input.readInt();
		
		for( int i = 0; i < size; i++){
			Long timestamp = input.readLong();
			TransitionEntry transition = kryo.readObject(input, TransitionEntry.class);
			entries.put(timestamp, transition);
		}
	}
	
	public TreeMap<Long, TransitionEntry> getEntries(){
		return entries;
	}
	
	public TransitionMap remainingMerged(TransitionMap v, TransitionType type)
	{
		TransitionMap ret = new TransitionMap();
		Iterator<Long> timestamps = v.entries.keySet().iterator();
		
	    while (timestamps.hasNext()) {
	    	Long timestamp = timestamps.next();
	    	TransitionEntry entry = v.entries.get(timestamp);
	    	
	    	this.entries.put(timestamp, entry);
	    	ret.entries.put(timestamp, entry);
	    	
	    	// check if all remain
	    	Entry<Long, TransitionEntry> lastEntry = this.entries.floorEntry(timestamp);
	    	
	    	while(true){
	    		Entry<Long, TransitionEntry> leftEntry = this.entries.lowerEntry(lastEntry.getKey());
	    		
	    		if( leftEntry == null || leftEntry.getValue().getIndex() == -1)
	    			break;
	    		else
	    			lastEntry = leftEntry;
	    	}
	    	
	    	Long lastEntryKey = lastEntry.getKey();
	    	
	    	if( this.entries.lowerEntry(lastEntryKey) == null ){
	    		if( type == TransitionType.TO || this.entries.higherEntry(lastEntryKey).getValue().getIndex() != 1 ){
	    			ret.entries.remove(lastEntryKey);
	    			this.entries.remove(lastEntryKey);
	    		}
	    	} else{
	    		Entry<Long, TransitionEntry> rightEntry = this.entries.higherEntry(lastEntryKey);
	    		if( rightEntry != null && rightEntry.getValue().getIndex() != -1){
		    		if( type == TransitionType.FROM){
		    			if( lastEntryKey > rightEntry.getKey()){
		    				ret.entries.remove(rightEntry.getKey());
			    			this.entries.remove(rightEntry.getKey());
		    			} else {
		    				ret.entries.remove(lastEntryKey);
			    			this.entries.remove(lastEntryKey);
		    			}
		    		} else if ( type == TransitionType.TO){
		    			ret.entries.remove(rightEntry.getKey());
		    			this.entries.remove(rightEntry.getKey());
		    		} else { // FROM_AND_TO
		    			lastEntry = rightEntry;
		    			// check one more to the right
		    			rightEntry = this.entries.higherEntry(lastEntry.getKey());
		    			if( rightEntry == null || rightEntry.getValue().getIndex() != -1){
		    				ret.entries.remove(lastEntry.getKey());
		    				this.entries.remove(lastEntry.getKey());
		    			}
		    		}
	    		}
	    	}
	    }
		
		return ret;
	}
}
