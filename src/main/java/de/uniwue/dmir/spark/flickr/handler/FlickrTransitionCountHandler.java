package de.uniwue.dmir.spark.flickr.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.spark.sql.SQLContext;
import org.eclipse.jetty.server.Request;

import de.uniwue.dmir.spark.flickr.task.FlickrTransitionCountTask;
import de.uniwue.dmir.swd.core.request.SparkRequestHandler;
import de.uniwue.dmir.swd.core.request.parameter.ResultParser;
import de.uniwue.dmir.swd.core.request.parameter.StringValue;
import de.uniwue.dmir.swd.core.task.data.DataFrameStorage;
import de.uniwue.dmir.swd.core.task.data.TaskData;

public class FlickrTransitionCountHandler extends SparkRequestHandler {
	private DataFrameStorage frames;
	private SQLContext sqlc;
	private String parquetPath;
	
	public FlickrTransitionCountHandler(SQLContext sqlc, DataFrameStorage frames, String parquetPath){

		this.sqlc = sqlc;
		this.parquetPath = parquetPath;
		this.frames = frames;

		bindableParameters.addDefault(new ResultParser());
		bindableParameters.addDefault(new StringValue("dataframe"));
	}

	public void handle(String arg0, Request arg1, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        
        try{
			TaskData td = new TaskData();
			td.put("dataframes", frames);
			td.put("sqlcontext", sqlc);
			td.put("parquetPath", parquetPath);
			FlickrTransitionCountTask gridCelltask = new FlickrTransitionCountTask(td);
			process(request, response, gridCelltask);

		} catch(Exception x){
			x.printStackTrace();
		}
	}
}