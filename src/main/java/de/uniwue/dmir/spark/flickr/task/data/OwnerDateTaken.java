package de.uniwue.dmir.spark.flickr.task.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import de.uniwue.dmir.swd.core.data.IMergeable;
import de.uniwue.dmir.swd.core.features.util.intervalizer.DegreeIntervalizer;


public class OwnerDateTaken implements IMergeable<OwnerDateTaken>, KryoSerializable, Serializable{

	private static final long serialVersionUID = -4526025914950887833L;
	private HashMap<String, TransitionMap> transitions;
	
	public OwnerDateTaken() {
		transitions = new HashMap<String, TransitionMap>();
	}
	
	public OwnerDateTaken(String owner, Timestamp datetaken) {
		this();
		TransitionMap timestamps = new TransitionMap();
		timestamps.getEntries().put(datetaken.getTime(), new TransitionEntry(datetaken));
		transitions.put(owner, timestamps);
	}
	
	public OwnerDateTaken(String owner, Timestamp datetaken, int index) {
		this();
		TransitionMap timestamps = new TransitionMap();
		timestamps.getEntries().put(datetaken.getTime(), new TransitionEntry(datetaken, index));
		transitions.put(owner, timestamps);
	}

	public void write(Kryo kryo, Output output) {
		Iterator<String> keys = transitions.keySet().iterator();
		output.writeInt(transitions.keySet().size());
	    while (keys.hasNext()) {
	    	String owner = keys.next();
	    	output.writeString(owner);

	    	kryo.writeObject(output, transitions.get(owner));
	    }
	}

	public void read(Kryo kryo, Input input) {
		
		int size = input.readInt();
		
		for( int i = 0; i < size; i++){
			String owner = input.readString();
			TransitionMap transitions_ = kryo.readObject(input, TransitionMap.class);
			transitions.put(owner, transitions_);
		}
	}
	
	public HashMap<String, TransitionMap> getTransitions(){
		return transitions;
	}
	
	public Grid toGrid(DegreeIntervalizer intervalizer){
		Grid grid = new Grid(intervalizer);
		
		Iterator<String> owners = transitions.keySet().iterator();
		while (owners.hasNext()) {
			String owner = owners.next();
			
			TransitionMap ownerTransitionEndpoints = transitions.get(owner);
			Iterator<Long> timestamps = ownerTransitionEndpoints.getEntries().keySet().iterator();
			while (timestamps.hasNext()){
				Long timestamp = timestamps.next();
				int index = ownerTransitionEndpoints.getEntries().get(timestamp).getIndex();
				grid.merge(index);
			}
	    }
		
		return grid;
	}

	public OwnerDateTaken merge(OwnerDateTaken other) {
		Iterator<String> otherowners = other.transitions.keySet().iterator();
	    while (otherowners.hasNext()) {
	    	String otherowner = otherowners.next();
	    	
	    	if( !this.transitions.containsKey(otherowner) ){
	    		transitions.put(otherowner, new TransitionMap());
	    	}
	    	
	    	TransitionMap othertimestamps = other.transitions.get(otherowner);
	    	Iterator<Long> othertimestampkeys = othertimestamps.getEntries().keySet().iterator();
	    	while (othertimestampkeys.hasNext()){
	    		Long timestamp = othertimestampkeys.next();
	    		if( !this.transitions.get(otherowner).getEntries().containsKey(timestamp)){
	    			this.transitions.get(otherowner).getEntries().put(timestamp, othertimestamps.getEntries().get(timestamp));
	    		}
	    	}
	    }
		
		return this;
	}
	
	public OwnerDateTaken remainingMerged(OwnerDateTaken v, TransitionMap.TransitionType type)
	{
		OwnerDateTaken ret = new OwnerDateTaken();
		
		Iterator<String> otherowners = v.transitions.keySet().iterator();
	    while (otherowners.hasNext()) {
	    	String otherowner = otherowners.next();
	    	
	    	TransitionMap othertimestamps = v.transitions.get(otherowner);
	    	TransitionMap remaining = transitions.get(otherowner).remainingMerged(othertimestamps, type);
	    	
	    	if( !remaining.getEntries().isEmpty()){
	    		ret.getTransitions().put(otherowner, remaining);
	    	}
	    	
	    }
		
		return ret;
	}

}
